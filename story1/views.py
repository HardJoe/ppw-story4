from django.shortcuts import render

# Create your views here.

def index(request):
    context = {
        "type" : "story1"
    }
    return render(request, "story1/index.html", context)
