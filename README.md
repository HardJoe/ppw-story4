# Jojo's Adventure
PPW Odd 2020/2021 - Story 4

## Overview
Jojo's Adventure shows his adventure in web designing (Story 2), front end developing (Story 3), and Django introduction. It is another Jojo's profile which shows his ability in professional aspects. You can see what kinds of programming skills, languages, projects, and organizations he's had in recent years. This website implements HTML/CSS with the help of Bootstrap, hence the grid layout. Gitlab CI/CD is still utilized for deployment process.

## Link
Click <http://jojostory4.herokuapp.com> to get to the website.

## Resources
- HTML
- CSS
- Bootstrap
- Django
- Heroku