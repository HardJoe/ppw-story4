from django.shortcuts import render

# Create your views here.

def index(request):
    context = {
        "type" : "index"
    }
    return render(request, "home/index.html", context)

def extras(request):
    context = {
        "type" : "extras"
    }
    return render(request, "home/extras.html", context)
